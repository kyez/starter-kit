import { Product } from "@app/models/product.model";

const products: Product[] = [
    {
        id: 1,
        name: "Koszulka",
        desc: "fdhsjf sdhfshkdfhskjdhf jshdkjfhdjhfk dfhskjhdf  sdhfkjsdhf",
        price: 19.99,
        bg_url: "brenner-20-velvet-pillows"
    },
    {
        id: 2,
        name: "Poduszka 'Mis'",
        desc: "fdhsjf sdhfshkdfhskjdhf jshdkjfhdjhfk dfhskjhdf  sdhfkjsdhf",
        price: 49.99,
        bg_url: "hilton-down-alternative-pillow-HIL-PillowDownAlt_xlrg"
    },
    {
        id: 3,
        name: "Poduszka 'Angela'",
        desc: "fdhsjf sdhfshkdfhskjdhf jshdkjfhdjhfk dfhskjhdf  sdhfkjsdhf",
        price: 49.99,
        bg_url: "westin-hotel-feather-down-pillow-HB-108_lrg"
    },
    {
        id: 4,
        name: "Poduszka",
        desc: "fdhsjf sdhfshkdfhskjdhf jshdkjfhdjhfk dfhskjhdf  sdhfkjsdhf",
        price: 49.99,
        bg_url: "brenner-20-velvet-pillows"
    },
    {
        id: 5,
        name: "Poduszka",
        desc: "fdhsjf sdhfshkdfhskjdhf jshdkjfhdjhfk dfhskjhdf  sdhfkjsdhf",
        price: 49.99,
        bg_url: "hilton-down-alternative-pillow-HIL-PillowDownAlt_xlrg"
    },
    {
        id: 6,
        name: "Poduszka",
        desc: "fdhsjf sdhfshkdfhskjdhf jshdkjfhdjhfk dfhskjhdf  sdhfkjsdhf",
        price: 49.99,
        bg_url: "westin-hotel-feather-down-pillow-HB-108_lrg"
    },
    {
        id: 7,
        name: "Poduszka",
        desc: "fdhsjf sdhfshkdfhskjdhf jshdkjfhdjhfk dfhskjhdf  sdhfkjsdhf",
        price: 49.99,
        bg_url: "brenner-20-velvet-pillows"
    },
    {
        id: 8,
        name: "Poduszka",
        desc: "fdhsjf sdhfshkdfhskjdhf jshdkjfhdjhfk dfhskjhdf  sdhfkjsdhf",
        price: 49.99,
        bg_url: "westin-hotel-feather-down-pillow-HB-108_lrg"
    },
    {
        id: 9,
        name: "Poduszka",
        desc: "fdhsjf sdhfshkdfhskjdhf jshdkjfhdjhfk dfhskjhdf  sdhfkjsdhf",
        price: 49.99,
        bg_url: "hilton-down-alternative-pillow-HIL-PillowDownAlt_xlrg"
    },
    {
        id: 10,
        name: "Poduszka",
        desc: "fdhsjf sdhfshkdfhskjdhf jshdkjfhdjhfk dfhskjhdf  sdhfkjsdhf",
        price: 49.99,
        bg_url: "brenner-20-velvet-pillows"
    },    
    {
        id: 11,
        name: "Poduszka",
        desc: "fdhsjf sdhfshkdfhskjdhf jshdkjfhdjhfk dfhskjhdf  sdhfkjsdhf",
        price: 49.99,
        bg_url: "westin-hotel-feather-down-pillow-HB-108_lrg"
    }
];

export default products;