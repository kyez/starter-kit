import { Action } from "@ngrx/store";
import { appState } from "@app/store/interfaces/appState.interface";

export const ACTION_LOGOUT = "[App] Logout";
export const ACTION_LOGIN = "[App] Login";

export class ActionLogout implements Action {
    readonly type = ACTION_LOGOUT;
}

export class ActionLogin implements Action {
    readonly type = ACTION_LOGIN;
}

export type Actions = ActionLogout | ActionLogin