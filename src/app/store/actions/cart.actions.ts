import { Action } from "@ngrx/store";
import { cartState } from "@app/store/interfaces/cartState.interface";
import { Product } from "@app/models/product.model";

export const ACTION_ADD_TO_CART = "[Cart] Add to Cart";
export const ACTION_REMOVE_FROM_CART = "[Cart] Remove from Cart";

export class ActionAdd implements Action {
    readonly type = ACTION_ADD_TO_CART;

    constructor(public payload: Product){}
}

export class ActionRemove implements Action {
    readonly type = ACTION_REMOVE_FROM_CART;

    constructor(public payload: Product){}  
}

export type Actions = ActionAdd | ActionRemove