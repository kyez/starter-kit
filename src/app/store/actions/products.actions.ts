import { Action } from "@ngrx/store";
import { productsState } from "@app/store/interfaces/productsState.interface";
import {Product} from '@app/models/product.model';

export const ADD_PRODUCT = '[Products] Add';
export const INIT_PRODUCTS = '[Products] Init';

export class InitProducts implements Action {
  readonly type = INIT_PRODUCTS;

  constructor(public payload: productsState){}
}

export class AddProduct implements Action {
    readonly type = ADD_PRODUCT;

    constructor(public payload: productsState){}
}

export type Actions = AddProduct | InitProducts
