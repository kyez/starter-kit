import { Action } from "@ngrx/store";
import { userState } from "../interfaces/userState.interface";

export const ACTION_SET_USER = "[User] Set user";

export class ActionSetUser implements Action {
    readonly type = ACTION_SET_USER;

    constructor(public payload: userState){}
}

export type Actions = ActionSetUser