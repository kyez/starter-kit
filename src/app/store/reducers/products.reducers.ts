import { Action } from "@ngrx/store";
import { productsState } from "@app/store/interfaces/productsState.interface";
import * as ProductsActions from "@app/store/actions/products.actions"


const initialState: productsState = {
  products: []
};

export function productsReducers(state = initialState, action: ProductsActions.Actions) {
    switch(action.type) {
      case ProductsActions.INIT_PRODUCTS:
          return action.payload;
    }
}
