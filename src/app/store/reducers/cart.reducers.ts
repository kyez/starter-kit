import { Action } from "@ngrx/store";
import { cartState } from "@app/store/interfaces/cartState.interface";
import * as CartActions from "@app/store/actions/cart.actions"

const initialState: cartState = {
    items: [
        {
            item: {
                id: 3,
                name: "dddd",
                desc: "sssss dddd",
                price: 12.22,
                bg_url: "hilton-down-alternative-pillow-HIL-PillowDownAlt_xlrg"
            },
            quantity: 2
        }   
    ], 
    sum: 24.44
}

export function cartReducers(state = initialState, action: CartActions.Actions) {
    switch (action.type){

        case CartActions.ACTION_ADD_TO_CART: 

            let itemFound = state.items.find( (el) => {
                return el.item.id === action.payload.id
            })            

            let newState: cartState;
            if(itemFound) {
                newState = {
                    items: [
                        ...state.items
                    ],
                    sum: state.sum
                }

                newState.items.map((el:any) => {
                    if(el.item.id === itemFound.item.id) {
                        el.quantity++;
                        newState.sum += el.item.price;
                    }
                })
            } else {
                newState = {
                    items: [
                        ...state.items,
                        {
                            item: action.payload,
                            quantity: 1
                        }
                    ],
                    sum: action.payload.price
                }

                newState.sum += state.sum;
            }

            newState.sum = Number(newState.sum.toFixed(2));

            return Object.assign(state, newState);

        case CartActions.ACTION_REMOVE_FROM_CART:
            newState = {
                ...state
            }

            let itemFoundIndex = state.items.findIndex( (el) => {
                return el.item.id === action.payload.id
            });

            newState.sum -= state.items[itemFoundIndex].item.price;
            newState.sum = Number(newState.sum.toFixed(2));

            if(state.items[itemFoundIndex].quantity > 1){
                newState.items[itemFoundIndex].quantity--;
            } else {
                newState.items.splice(itemFoundIndex, 1);                
            }

            return newState;     
    }

    return state;
}
