import { Action } from "@ngrx/store";
import * as AppActions from "@app/store/actions/app.actions"
import { appState } from "@app/store/interfaces/appState.interface";

const initialState: appState = {
    logged: false,
    theme: 'dark',
}

export function appReducer(state = initialState, action: AppActions.Actions) {
    switch (action.type){
        case AppActions.ACTION_LOGOUT: 
            return initialState;
        case AppActions.ACTION_LOGIN: 
            return {
                ...state,
                logged: true    
            }     
    }

    return state;
}