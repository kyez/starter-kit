import { userReducer } from "@app/store/reducers/user.reducers";
import { ActionReducerMap } from "@ngrx/store";
import { AppState } from "../../app.state";
import { productsReducers } from "@app/store/reducers/products.reducers";
import { appReducer } from "@app/store/reducers/app.reducers";
import { cartReducers } from "@app/store/reducers/cart.reducers";

export const reducers: ActionReducerMap<AppState> = {
    appState: appReducer,
    userState: userReducer,
    productsState: productsReducers,
    cartState: cartReducers
}