import { userState } from "@app/store/interfaces/userState.interface";
import { Action} from "@ngrx/store";
import * as UserActions from "@app/store/actions/user.actions";

const initialState: userState = {
    username: "Guest",
    id: 0
};

export function userReducer(state = initialState, action: UserActions.Actions) {
    switch (action.type) {
        case UserActions.ACTION_SET_USER: {
            return Object.assign(state, action.payload);
        }
    }    

    return state;
};