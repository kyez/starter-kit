export interface productsState {
  products:
      {
        id: number,
        price: number,
        name: string
      }[]
}
