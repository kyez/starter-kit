import { userState } from "@app/store/interfaces/userState.interface";

export interface appState {
    logged: boolean,
    theme: string
}