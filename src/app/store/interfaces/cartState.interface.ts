import { Product } from "@app/models/product.model";

export interface cartState {
    items: {
        item: Product,
        quantity: number
    }[],
    sum: number
}