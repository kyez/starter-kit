import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { ServiceWorkerModule } from "@angular/service-worker";
import { TranslateModule } from "@ngx-translate/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { environment } from "@env/environment";
import { SharedModule } from "@app/shared";
import { AboutModule } from "./components/about/about.module";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { HomeComponent } from "@app/components/home/home.component";
import { AdminHomeComponent } from "@app/components/admin/home/home.component";

import { ProductsComponent } from "@app/components/products/products.component";
import { NavigationComponent } from "@app/components/navigation/navigation.component";
import { ProductsListComponent } from "@app/components/products/products-list/products-list.component";
import { HeaderComponent } from "@app/components/header/header.component";

import { ProductDetailsComponent } from "@app/components/products/product-details/product-details.component";
import { HomeService } from "@app/services/home.service";
import { ProductsService } from "@app/services/products.service";
import { LoginService } from "@app/services/login.service";

import { CartSnippetService } from "@app/services/cart-snippet.service";

import { CartSnippetComponent } from "@app/components/cart-snippet/cart-snippet.component";

import { StoreModule } from "@ngrx/store";
import { reducers } from "./store/reducers";
import { LoginComponent } from "./components/login/login.component";

import { CommonModule } from "@angular/common";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { ToastrModule } from "ngx-toastr";
import { AdminGuard } from "@app/guards/admin.guard";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";

@NgModule({
	imports: [
		BrowserModule,
		StoreModule.forRoot(reducers, {}),
		StoreDevtoolsModule.instrument({}),
		ServiceWorkerModule.register("./ngsw-worker.js", { enabled: environment.production }),
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		TranslateModule.forRoot(),
		NgbModule.forRoot(),
		SharedModule,
		AboutModule,
		AppRoutingModule,
		CommonModule,
		BrowserAnimationsModule, // required animations module
		ToastrModule.forRoot(), // ToastrModule added
	],
	declarations: [
		AppComponent,
		HomeComponent,
		ProductsComponent,
		ProductDetailsComponent,
		ProductsListComponent,
		NavigationComponent,
		CartSnippetComponent,
		HeaderComponent,
		AdminHomeComponent,
		LoginComponent,
	],
	providers: [HomeService, ProductsService, CartSnippetService, LoginService, AdminGuard],
	bootstrap: [AppComponent],
})
export class AppModule {}
