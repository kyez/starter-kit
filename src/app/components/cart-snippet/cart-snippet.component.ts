import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { CartSnippetService } from '@app/services/cart-snippet.service';
import { Route, Router, ActivatedRoute } from '@angular/router';
import products from '@app/db/products';
import { Product } from '@app/models/product.model';

@Component({
  selector: 'app-cart-snippet',
  templateUrl: './cart-snippet.component.html',
  styleUrls: ['./cart-snippet.component.scss']
})
export class CartSnippetComponent implements OnInit {
  //Lista produktów
  quote: string;
  isLoading: boolean;
  productsItems: Product[];
  cart: any;

  constructor(private service: CartSnippetService,
    private router: Router,
    private activatedRoute: ActivatedRoute,) {}

  ngOnInit() {
    this.service.getCart().subscribe(cart =>{      
      this.cart = cart;
    });
  }

  removeFromCart(product:Product) {
    this.service.removeFromCart(product);
  }
  naviateToProduct(product:Product) {
    this.router.navigate([`products/${product.id}`])
  }

}
