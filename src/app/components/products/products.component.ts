import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { ProductsService } from '@app/services/products.service';
import { Route, Router, ActivatedRoute } from '@angular/router';
import products from '@app/db/products';
import { Product } from '@app/models/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  //Lista produktów
  quote: string;
  isLoading: boolean;
  productsItems: Product[];

  constructor(private productsService: ProductsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,) {}

  ngOnInit() {
    this.productsItems = products;
  }

  naviateToProduct(product:Product) {
    this.router.navigate([`products/${product.id}`])
  }

  addToCart(product: Product) {
    this.productsService.addToCart(product);
    // this.toastr.success('Added to Cart');
    // this.homeService.getCartState().subscribe( (store:cartState) => {
    //   console.log(store);
    // })
  }


}
