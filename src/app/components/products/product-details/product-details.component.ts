import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { ProductsService } from '@app/services/products.service';
import { Route, Router, ActivatedRoute } from '@angular/router';
import products from '@app/db/products';
import { Product } from '@app/models/product.model';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  quote: string;
  isLoading: boolean;
  currentProductId: number
  currentProduct: Product;
  products: Product[] = products;

  constructor(private productsService: ProductsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,) {}

  ngOnInit() {    
    this.currentProductId = Number(this.activatedRoute.snapshot.params["id"]);
    this.currentProduct = this.products.find((product) => {
      return product.id === this.currentProductId;
    });
    
    console.log(this.currentProduct);
    
  }

}
