import { Component, OnInit } from "@angular/core";
import { finalize } from "rxjs/operators";
import { ProductsService } from "@app/services/products.service";
import { Route, Router, ActivatedRoute } from "@angular/router";
import products from "@app/db/products";
import { Product } from "@app/models/product.model";
import { ToastrService } from "ngx-toastr";

@Component({
	selector: "app-products-list",
	templateUrl: "./products-list.component.html",
	styleUrls: ["./products-list.component.scss"],
})
export class ProductsListComponent implements OnInit {
	quote: string;
	isLoading: boolean;
	productsItems: Product[];

	constructor(
		private productsService: ProductsService,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private toastr: ToastrService
	) {
		this.productsService.getProducts();
	}

	ngOnInit() {
		this.productsService.getProductsStore().subscribe(products => {
			console.log("ProductsListComponent :: products", products);
			if(products){
        this.productsItems = products.products;
      }
		});
		// console.log(this.productsService.getProducts());
	}

	naviateToProduct(product: Product) {
		this.router.navigate([`products/${product.id}`]);
	}

	addToCart(product: Product) {
		this.productsService.addToCart(product);
		this.toastr.success("Dodano do koszyka");
	}
}
