import { Component, OnInit } from "@angular/core";
import { finalize } from "rxjs/operators";
import { Route, Router, ActivatedRoute } from "@angular/router";

@Component({
	selector: "app-header",
	templateUrl: "./header.component.html",
	styleUrls: ["./header.component.scss"],
})
export class HeaderComponent {
	constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

	navigateToLogin() {
		this.router.navigate([`login/`]);
	}

	isLogged() {
		return localStorage.getItem("token");
	}

	toggleCart() {
		document.querySelector(".cart").classList.toggle("show");
	}
}
