import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { LoginService } from "@app/services/login.service";

@Component({
	selector: "app-login",
	templateUrl: "./login.component.html",
	styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
	error: string;
	loginForm: FormGroup;
	isLoading = false;

	constructor(private router: Router, private formBuilder: FormBuilder, private service: LoginService) {
		this.createForm();
	}

	ngOnInit() {}

	login() {
		this.isLoading = true;
		this.service.logIn({
			username: this.loginForm.controls["username"].value,
			password: this.loginForm.controls["password"].value,
		});
	}

	private createForm() {
		this.loginForm = this.formBuilder.group({
			username: ["", Validators.required],
			password: ["", Validators.required],
			remember: true,
		});
	}
}
