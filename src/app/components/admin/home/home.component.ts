import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { HomeService } from '@app/services/home.service';
import { userState } from '../../../store/interfaces/userState.interface'
import { appState } from '@app/store/interfaces/appState.interface';

import products from '@app/db/products';
import { Product } from '@app/models/product.model';
import { Router, ActivatedRoute } from '@angular/router';
import { cartState } from '@app/store/interfaces/cartState.interface';

import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class AdminHomeComponent implements OnInit {

  quote: string;
  isLoading: boolean;
  productsItems: Product[] = products; 

  constructor(private homeService: HomeService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService) {}

  ngOnInit() {}

  logIn(){
    console.log("logowanie");
    let tempUserData: userState = {
      id: 1,
      username: "admin"
    }

    this.homeService.logIn(tempUserData);
    this.getUserLogin();
  }

  getUserLogin() {
    this.homeService.getUserState().subscribe( (store:userState) => {
      console.log(store);
    })

    this.homeService.getAppState().subscribe( (store:appState) => {
      console.log(store);
    })
  }

  naviateToProduct(product:Product) {
    this.router.navigate([`products/${product.id}`])
  }

  addToCart(product: Product) {
    this.homeService.addToCart(product);
    this.toastr.success('Added to Cart');
    this.homeService.getCartState().subscribe( (store:cartState) => {
      console.log(store);
    })
  }
}
