import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Route, Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute) {}

  navigateToProducts() {    
    this.router.navigate([`products/`]);
  }

  navigateToHome(){
    this.router.navigate([`/`]);
  }
}
