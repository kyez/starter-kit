import { userState } from "@app/store/interfaces/userState.interface";
import { productsState } from "@app/store/interfaces/productsState.interface";
import { appState } from "@app/store/interfaces/appState.interface";
import { cartState } from "@app/store/interfaces/cartState.interface";


export interface AppState {
    appState: appState,
    userState:  userState,
    productsState: productsState,
    cartState: cartState
}