import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Product } from '@app/models/product.model';
import { ActionAdd } from '@app/store/actions/cart.actions';
import { InitProducts } from '@app/store/actions/products.actions';

@Injectable()
export class ProductsService {
    constructor(private http: HttpClient,private store: Store<any>){}

    baseURL = "http://localhost/wordpress/index.php/wp-json";

    getProducts() {
      this.http.get(this.baseURL + "/wp/v2/products?_embed").subscribe(
        (res: any) => {

          let products: Product[] = [];

          res.forEach(el => {
            products.push({
              id: el.id,
              name: el.title.rendered,
              desc: el.content.rendered,
              price: Number(el.acf.price),
              bg_url: el.acf.obrazek1 ? el.acf.obrazek1.url : null
            })
          });

          this.store.dispatch(new InitProducts({products: products}));
        },
        err => {
          console.log("error", err);
        }
      );
    }

    getProductsStore() {
      return this.store.select('productsState');
    }

    addToCart(product: Product){
        this.store.dispatch(new ActionAdd(product));
    }

}
