import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Store } from "@ngrx/store";
import { ACTION_LOGIN, ActionLogin } from "@app/store/actions/app.actions";
import { userState } from "@app/store/interfaces/userState.interface";
import { ActionSetUser } from "@app/store/actions/user.actions";
import { Observable } from "rxjs";
import { Router } from "@angular/router";

@Injectable()
export class LoginService {
	constructor(private store: Store<any>, private http: HttpClient, private router: Router) {}

	baseURL = "http://localhost/wordpress/index.php/wp-json";

	logIn(userData: any) {
		this.http
			.post(this.baseURL + "/jwt-auth/v1/token", {
				username: userData.username,
				password: userData.password,
			})
			.subscribe(
				(res: { token: string; user_display_name: string }) => {
					localStorage.setItem("token", res.token);
					localStorage.setItem("username", res.user_display_name);

					this.getLoggedUser(res.user_display_name);
				},
				err => {
					console.log("error", err);
				}
			);
	}

	getLoggedUser(username: string) {
		this.http.get(this.baseURL + "/wp/v2/users").subscribe(
			(res: any) => {
				res.forEach((el: any) => {
					if (el.name == username) {
						this.doLoginAction({
							id: el.id,
							username: el.name,
						});
					}
				});
			},
			err => {
				console.log("error", err);
			}
		);
	}

	doLoginAction(userData: userState) {
		this.store.dispatch(new ActionLogin());
		this.store.dispatch(new ActionSetUser(userData));

		this.store.select("appState").subscribe(store => {
			console.log(store);
		});

		this.store.select("userState").subscribe(store => {
			console.log(store);
		});
	}
}
