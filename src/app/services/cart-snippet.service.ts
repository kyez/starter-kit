import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Product } from '@app/models/product.model';
import { ActionRemove } from '@app/store/actions/cart.actions';

@Injectable()
export class CartSnippetService {
    protected apiKey = "9D049EBD-B3BB-558A-FFD4-DBF87AD97400/FB06C29D-BDCF-78A3-FF76-976F645C5300"

    constructor(private http: HttpClient,private store: Store<any>){}
    
    getCart() {
        return this.store.select('cartState');
    }

    removeFromCart(product:Product){
        this.store.dispatch(new ActionRemove(product))
    }
}