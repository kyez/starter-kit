import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { ACTION_LOGIN, ActionLogin } from '@app/store/actions/app.actions';
import { userState } from '@app/store/interfaces/userState.interface';
import { Product } from '@app/models/product.model';
import { ActionAdd } from '@app/store/actions/cart.actions';

@Injectable()
export class HomeService {
    protected apiKey = "9D049EBD-B3BB-558A-FFD4-DBF87AD97400/FB06C29D-BDCF-78A3-FF76-976F645C5300"

    constructor(private http: HttpClient,
                private store: Store<any>){}

    getUserName() {
        console.log("getting user name");
        return this.http.get(`https://api.backendless.com/${this.apiKey}/data/Users`, {
            headers: {
                'content-type': 'application/json' 
            },
            observe: "body"
        });
    }

    logIn(userData: userState) {
        this.store.dispatch(new ActionLogin());
    }

    addToCart(product: Product){
        this.store.dispatch(new ActionAdd(product));
    }


    getCartState(){
        return this.store.select('cartState');
    }

    getUserState(){
        return this.store.select('userState');
    }

    getAppState(){
        return this.store.select('appState');
    }
}