import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "@app/components/home/home.component";
import { AppComponent } from "@app/app.component";
import { ProductsComponent } from "@app/components/products/products.component";
import { ProductDetailsComponent } from "@app/components/products/product-details/product-details.component";
import { AdminHomeComponent } from "@app/components/admin/home/home.component";
import { AdminGuard } from "@app/guards/admin.guard";
import { LoginComponent } from "@app/components/login/login.component";

const routes: Routes = [
	// Fallback when no prior route is matched
	{
		path: "",
		component: HomeComponent,
	},
	{
		path: "home",
		component: HomeComponent,
	},
	{
		path: "login",
		component: LoginComponent,
	},
	{
		path: "products",
		component: ProductsComponent,
	},
	{
		path: "products/:id",
		component: ProductDetailsComponent,
	},
	{
		path: "admin/home",
		component: AdminHomeComponent,
		canActivate: [AdminGuard],
	},
	{
		path: "**",
		redirectTo: "",
		pathMatch: "full",
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { useHash: true })],
	exports: [RouterModule],
	providers: [],
})
export class AppRoutingModule {}
