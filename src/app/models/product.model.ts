export interface Product {
    id: number,
    name: string,
    desc: string,
    price: number,
    bg_url: string   
}